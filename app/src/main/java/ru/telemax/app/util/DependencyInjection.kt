package ru.telemax.app.util

import android.content.Context
import okhttp3.logging.HttpLoggingInterceptor
import ru.telemax.app.di.ApplicationModule
import ru.telemax.app.di.Scopes
import toothpick.Toothpick
import toothpick.configuration.Configuration

object DependencyInjection {

    fun configure(context: Context) {
        Toothpick.setConfiguration(getConfig())
        initScopes(context)
    }

    private fun getConfig(): Configuration {
        return Configuration.forDevelopment().preventMultipleRootScopes()
    }

    private fun initScopes(context: Context) {
        val appModule = ApplicationModule(context,
                "mobileapp.hitel.ru",
                HttpLoggingInterceptor.Level.BODY)

        val appScope = Toothpick.openScope(Scopes.APP)
        appScope.installModules(appModule)
    }

}
