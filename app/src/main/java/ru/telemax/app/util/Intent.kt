package ru.telemax.app.util

import android.content.Intent
import android.content.Intent.*
import android.net.Uri


/**
 * Created by Elyseev Vladimir on 07.04.18.
 */
class Intents {

    companion object {
        fun openUrl(url: String) = Intent(ACTION_VIEW).also { it.data = Uri.parse(url) }

        fun openGoogle(search: String) = Intent(ACTION_VIEW).also { it.data = Uri.parse("http://www.google.com/#q=$search") }

        fun openCall(phone: String) = Intent(ACTION_CALL).also { it.data = Uri.parse("tel:$phone") }

        fun openEmailTech(body: String) = Intent(ACTION_SENDTO).also { it.data = Uri.parse(
                "mailto:telemaks@hitel.ru?subject=$body") }

        fun openEmailFeedback(data: Pair<String, String>) = Intent(ACTION_SENDTO).also { it.data = Uri.parse(
                "mailto:web@hitel.ru?subject=${data.first}&body=${Uri.encode(data.second)}") }

        fun openEmailSales(body: String) = Intent(ACTION_SENDTO).also { it.data = Uri.parse(
                "mailto:sales@hitel.ru?body=" + Uri.encode(body)) }

        fun openShare() = Intent(ACTION_SEND).also {
            it.type = "text/plain"
            it.putExtra(EXTRA_TEXT,
                    "Контакты Оператора Связи ООО «ТелеМакс»\n" +
                            "350020, г. Краснодар, ул. Бабушкина 252, 4 этаж;\n" +
                            "www.hitel.ru; sales@hitel.ru; +7 (861) 212-55-55; \n" +
                            "+7 (861) 212-50-00")
            createChooser(it, "Поделиться контактами")
        }

        fun openWhatsAppRequest(message: String) = Intent(ACTION_VIEW, Uri.parse(
                "https://api.whatsapp.com/send?" +
                        "phone=+79282042794&text=$message"))

        fun openWhatsApp() = Intent(ACTION_VIEW, Uri.parse(
                "https://api.whatsapp.com/send?" +
                        "phone=+79282042794&" +
                        "text=Здравствуйте!  Прошу помочь с решением вопроса: "))
    }
}
