package ru.telemax.app.util.extensions

import android.text.format.DateFormat
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.telemax.app.navigation.Screen
import ru.terrakok.cicerone.Router
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by Elyseev Vladimir on 29.03.18.
 */

fun Router.backTo(screen: Screen) {
    backTo(screen.name)
}

fun Router.newRootScreen(screen: Screen, data: Any? = null) {
    newRootScreen(screen.name, data)
}

fun Router.navigateTo(screen: Screen) {
    navigateTo(screen.name)
}

fun Router.navigateTo(screen: Screen, data: Any) {
    navigateTo(screen.name, data)
}

fun Router.newScreenChain(screen: Screen) {
    newScreenChain(screen.name)
}

fun Router.replaceScreen(screen: Screen) {
    replaceScreen(screen.name)
}

fun timer(delay: Long, func: () -> Unit) = Observable.timer(delay, TimeUnit.MILLISECONDS)
    .subscribeOn(Schedulers.io())
    .observeOn(AndroidSchedulers.mainThread())
    .subscribe { func.invoke() }

val Any?.json: String
    get() = Gson().toJson(this)

inline fun <reified T> Any.toObject(): T? = Gson().fromJson(this as String, T::class.java)

val Long.asShortDate: String
    get() = DateFormat.format("dd.MM.yyyy", Date(this * 1000)) as String

val Long.asDate: String
    get() = DateFormat.format("kk:mm, dd MMMM yyyy", Date(this * 1000)) as String
