package ru.telemax.app.util.extensions

import android.widget.EditText
import com.redmadrobot.inputmask.MaskedTextChangedListener

/**
 * Created by Elyseev Vladimir on 07.04.18.
 */

fun EditText.addMask(mask: String) {
    addTextChangedListener(MaskedTextChangedListener(mask, true, this, null, null))
}

fun EditText.addMaskPhone() {
    addTextChangedListener(MaskedTextChangedListener("+{7} ([000]) [000]-[00]-[00]", true, this, null, null))
}

fun EditText.addMaskNumber() {
    addTextChangedListener(MaskedTextChangedListener("[000]-[000]-[000]", true, this, null, null))
}

fun EditText.text() = text.toString()
