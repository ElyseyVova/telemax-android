package ru.telemax.app.util.extensions

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by Elyseev Vladimir on 17.04.18.
 */
fun <T> Observable<T>.onUiThread(): Observable<T> = this.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())

fun <T> Single<T>.onUiThread(): Single<T> = this.subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())

fun Disposable.addToComposite(disposable: CompositeDisposable) {
    disposable.add(this)
}
