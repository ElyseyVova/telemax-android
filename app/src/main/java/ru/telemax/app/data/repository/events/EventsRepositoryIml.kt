package ru.telemax.app.data.repository.events

import io.reactivex.disposables.CompositeDisposable
import ru.telemax.app.data.model.Event
import ru.telemax.app.data.retrofit.TelemaxApi
import ru.telemax.app.util.extensions.addToComposite
import ru.telemax.app.util.extensions.onUiThread

/**
 * Created by Elyseev Vladimir on 07.04.18.
 */
class EventsRepositoryIml(private val telemaxApi: TelemaxApi) : EventsRepository {

    val AUTH_TOKEN_QUERY = "auth_token"
    val PAGE_QUERY = "page"
    val TOKEN = "9Q0NdidI3V8z7DiNcFvdSQ"

    override fun getEvents(
            page: Int,
            onStart: () -> Unit,
            onSuccess: (List<Event>) -> Unit,
            onError: () -> Unit,
            onFinally: () -> Unit,
            addDisposables: CompositeDisposable) {

        val params = mapOf(AUTH_TOKEN_QUERY to TOKEN, PAGE_QUERY to "$page")

        telemaxApi.getEvents(params)
                .onUiThread()
                .doOnSubscribe { onStart.invoke() }
                .doFinally { onFinally.invoke() }
                .subscribe(
                        {
                            when {
                                it.events != null -> onSuccess.invoke(it.events!!)
                                else -> onError.invoke()
                            }
                        },
                        { onError.invoke() })
                .addToComposite(addDisposables)
    }
}
