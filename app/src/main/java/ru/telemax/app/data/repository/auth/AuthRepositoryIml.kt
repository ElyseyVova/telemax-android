package ru.telemax.app.data.repository.auth

import ru.telemax.app.data.store.AuthStore

/**
 * Created by Elyseev Vladimir on 07.04.18.
 */
class AuthRepositoryIml(private val authStore: AuthStore) : AuthRepository {

    override val isNotAuthorized: Boolean
        get() = authStore.profile == null

    override var isFirstLaunch: Boolean
        get() = authStore.isFirstLaunch
        set(value) {
            authStore.isFirstLaunch = value
        }
}
