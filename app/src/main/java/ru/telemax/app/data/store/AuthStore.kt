package ru.telemax.app.data.store

import ru.telemax.app.data.model.Profile

/**
 * Created by Elyseev Vladimir on 02.04.18.
 */
interface AuthStore {

    var isFirstLaunch: Boolean

    var profile: Profile?
}
