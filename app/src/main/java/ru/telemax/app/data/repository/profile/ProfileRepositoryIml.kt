package ru.telemax.app.data.repository.profile

import io.reactivex.disposables.CompositeDisposable
import ru.telemax.app.data.model.Profile
import ru.telemax.app.data.retrofit.TelemaxApi
import ru.telemax.app.data.store.AuthStore
import ru.telemax.app.util.extensions.addToComposite
import ru.telemax.app.util.extensions.onUiThread

/**
 * Created by Elyseev Vladimir on 07.04.18.
 */
class ProfileRepositoryIml(
    private val telemaxApi: TelemaxApi,
    private val authStore: AuthStore
) : ProfileRepository {

    override var profile: Profile
        get() = authStore.profile ?: Profile()
        set(value) {
            authStore.profile = value
        }

    val AUTH_TOKEN_QUERY = "auth_token"
    val TOKEN = "9Q0NdidI3V8z7DiNcFvdSQ"

    override fun getStatus(
        onStart: () -> Unit,
        onSuccess: (Boolean) -> Unit,
        onError: () -> Unit,
        onFinally: () -> Unit,
        addDisposables: CompositeDisposable) {

        val params = mapOf(AUTH_TOKEN_QUERY to TOKEN)

        telemaxApi.getStatus(params)
            .onUiThread()
            .doOnSubscribe { onStart.invoke() }
            .doFinally { onFinally.invoke() }
            .subscribe(
                {
                    onSuccess.invoke(it.isWorks ?: false)
                },
                { onError.invoke() })
            .addToComposite(addDisposables)
    }
}
