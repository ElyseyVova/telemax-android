package ru.telemax.app.data.model.response

import com.google.gson.annotations.SerializedName

/**
 * Created by Elyseev Vladimir on 07.04.18.
 */
class StatusResponse(@SerializedName("technical_works") var isWorks: Boolean?)
