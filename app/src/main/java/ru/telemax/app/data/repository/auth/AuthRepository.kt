package ru.telemax.app.data.repository.auth

/**
 * Created by Elyseev Vladimir on 07.04.18.
 */
interface AuthRepository {

    val isNotAuthorized: Boolean

    var isFirstLaunch: Boolean

}
