package ru.telemax.app.data.repository.profile

import io.reactivex.disposables.CompositeDisposable
import ru.telemax.app.data.model.Profile

/**
 * Created by Elyseev Vladimir on 07.04.18.
 */
interface ProfileRepository {

    var profile: Profile

    fun getStatus(onStart: () -> Unit,
                  onSuccess: (Boolean) -> Unit,
                  onError: () -> Unit,
                  onFinally: () -> Unit,
                  addDisposables: CompositeDisposable
    )
}
