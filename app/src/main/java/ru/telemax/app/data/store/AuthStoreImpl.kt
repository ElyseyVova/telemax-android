package ru.telemax.app.data.store

import android.content.SharedPreferences
import ru.telemax.app.data.model.Profile
import ru.telemax.app.util.extensions.json
import ru.telemax.app.util.extensions.toObject

/**
 * Created by Elyseev Vladimir on 02.04.18.
 */
class AuthStoreImpl(private val pref: SharedPreferences) : AuthStore {

    companion object {
        private const val PREFS_FIRST_LAUNCH = "prefs.splash.first_launch"
        private const val PREFS_PROFILE = "prefs.auth.profile"
    }

    override var isFirstLaunch: Boolean
        get() = pref.getBoolean(PREFS_FIRST_LAUNCH, false)
        set(value) = pref.edit().putBoolean(PREFS_FIRST_LAUNCH, value).apply()

    override var profile: Profile?
        get() = pref.getString(PREFS_PROFILE, null)?.toObject<Profile>()
        set(value) = pref.edit().putString(PREFS_PROFILE, value.json).apply()
}
