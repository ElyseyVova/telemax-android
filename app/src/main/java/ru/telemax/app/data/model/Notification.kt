package ru.telemax.app.data.model

import java.io.Serializable

data class Notification(var message: String) : Serializable
