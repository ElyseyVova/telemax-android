package ru.telemax.app.data.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.NotificationManager.*
import android.app.PendingIntent
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.support.v4.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.jetbrains.anko.notificationManager
import ru.telemax.app.R
import ru.telemax.app.ui.screens.home.HomeActivity
import ru.telemax.app.ui.screens.splash.SplashActivity
import timber.log.Timber


class MessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        if (message.data.isNotEmpty()) {
            sendNotification(message.data["title"]!!, message.data["notification"]!!)
            Timber.d("Received $message")
        } else {
            Timber.d("No data received!")
        }
    }

    fun sendNotification(title: String, message: String) {
        val intent = Intent(this, SplashActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.putExtra("from_notification", true)

        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        val soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        val notificationBuilder = NotificationCompat.Builder(this, "telemax_channel")
                .setSmallIcon(R.drawable.ic_notifications)
                .setColor(resources.getColor(R.color.colorAccent))
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(soundUri)
                .setContentIntent(pendingIntent);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager.createNotificationChannel(NotificationChannel("telemax_channel", resources.getString(R.string.app_name), IMPORTANCE_HIGH))
            notificationManager.notify(0, notificationBuilder.build())
        } else {
            notificationManager.notify(0, notificationBuilder.build())
        }
    }
}
