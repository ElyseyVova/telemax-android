package ru.telemax.app.data.retrofit

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.QueryMap
import ru.telemax.app.data.model.response.EventsResponse
import ru.telemax.app.data.model.response.StatusResponse

/**
 * Created by Elyseev Vladimir on 02.04.18.
 */
interface TelemaxApi {

    @GET("api/v1/news.json")
    fun getEvents(@QueryMap params: Map<String, String>): Single<EventsResponse>

    @GET("api/v1/statuses.json")
    fun getStatus(@QueryMap params: Map<String, String>): Single<StatusResponse>
}
