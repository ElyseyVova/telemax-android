package ru.telemax.app.data.repository.events

import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import ru.telemax.app.data.model.Event
import ru.telemax.app.data.model.response.EventsResponse

/**
 * Created by Elyseev Vladimir on 07.04.18.
 */
interface EventsRepository {

    fun getEvents(page: Int,
                  onStart: () -> Unit,
                  onSuccess: (List<Event>) -> Unit,
                  onError: () -> Unit,
                  onFinally: () -> Unit,
                  addDisposables: CompositeDisposable)
}
