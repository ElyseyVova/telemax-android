package ru.telemax.app.data.model

/**
 * Created by Elyseev Vladimir on 07.04.18.
 */
class Profile {
    var subscriber: String = ""
    var name: String = ""
    var email: String = ""
    var phone: String = ""
    var number: String = ""
    var manager: String = ""
    var phoneManager: String = ""
    var phoneWhatsApp: String = ""
}
