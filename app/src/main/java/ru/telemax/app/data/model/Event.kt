package ru.telemax.app.data.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Elyseev Vladimir on 30.03.18.
 */
class Event() {

    @SerializedName("id")
    var id: Int = 0

    @SerializedName("heading")
    var title: String = ""
        get() {
            var formattedTitle = field.replace("<p>", "")
            formattedTitle = formattedTitle.replace("</p>", "")
            return formattedTitle
        }

    @SerializedName("body")
    var body: String = ""
        get() {
            var formattedName = field.replace("<p>", "")
            formattedName = formattedName.replace("</p>", "")
            return formattedName
        }

    @SerializedName("basic_text")
    var basicText: String = ""

    @SerializedName("created_at")
    var date: Long = 0L

    var isExpanded = false
}
