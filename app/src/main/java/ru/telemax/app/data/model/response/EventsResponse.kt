package ru.telemax.app.data.model.response

import com.google.gson.annotations.SerializedName
import ru.telemax.app.data.model.Event

/**
 * Created by Elyseev Vladimir on 07.04.18.
 */
class EventsResponse(@SerializedName("news") var events: List<Event>?)
