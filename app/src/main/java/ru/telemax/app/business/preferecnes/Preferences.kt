package ru.telemax.app.business.preferecnes

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by Elyseev Vladimir on 29.03.18.
 */
class Preferences(context: Context) {

    val PREFS_FILENAME = "telemax_applciation"
    val FIRST_LAUNCH = "first_launch"
    val AUTHORIZED = "is_authorized"

    val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, 0);

    var isFirstLaunch: Boolean
        get() = prefs.getBoolean(FIRST_LAUNCH, true)
        set(value) = prefs.edit().putBoolean(FIRST_LAUNCH, value).apply()

    var isAuthorized: Boolean
        get() = prefs.getBoolean(AUTHORIZED, false)
        set(value) = prefs.edit().putBoolean(AUTHORIZED, value).apply()
}
