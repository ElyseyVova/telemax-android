package ru.telemax.app.business.repository

/**
 * Created by Elyseev Vladimir on 29.03.18.
 */
interface Repository {

    var isFirstLaunch: Boolean
    var isAuthorized: Boolean
}
