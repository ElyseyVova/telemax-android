package ru.telemax.app.business.repository

import ru.telemax.app.business.preferecnes.Preferences

/**
 * Created by Elyseev Vladimir on 29.03.18.
 */
class RepositoryImpl(private val preferences: Preferences) : Repository {

    override var isFirstLaunch: Boolean
        set(value) {
            preferences.isFirstLaunch = value
        }
        get() = preferences.isFirstLaunch


    override var isAuthorized: Boolean
        set(value) {
            preferences.isAuthorized = value
        }
        get() = preferences.isAuthorized

}
