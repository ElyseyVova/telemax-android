package ru.telemax.app.ui.screens.home.feedback

import android.content.res.Resources
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.telemax.app.R
import ru.telemax.app.data.repository.profile.ProfileRepository
import ru.telemax.app.navigation.Screen
import ru.telemax.app.util.extensions.navigateTo
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class FeedbackPresenter @Inject constructor(
        private val profileRepository: ProfileRepository,
        private val resources: Resources,
        private val router: Router
) : MvpPresenter<FeedbackView>() {

    private var profile = profileRepository.profile

    fun handleSendClick(text: String) {
        when {
            text.isEmpty() -> router.showSystemMessage(resources.getString(R.string.feedback_error_empty))
            else -> router.navigateTo(Screen.EMAIL_FEEDBACK_SCREEN, Pair("Отзыв  ${profile.subscriber}, мобильное приложение", text))
        }
    }

    fun handleFormClick() {
        router.navigateTo(Screen.WEB_SCREEN, resources.getString(R.string.feedback_anketa_url))
    }
}
