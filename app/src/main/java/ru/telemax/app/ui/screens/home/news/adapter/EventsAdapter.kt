package ru.telemax.app.ui.screens.home.news.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import ru.telemax.app.data.model.Event
import ru.telemax.app.ui.screens.home.events.adapter.EventVH

/**
 * Created by Elyseev Vladimir on 30.03.18.
 */
class EventsAdapter : RecyclerView.Adapter<EventVH>() {

    var onEventClick: (Event) -> Unit = {}

    var data: List<Event> = mutableListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount() = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = EventVH(parent)

    override fun onBindViewHolder(holder: EventVH, position: Int) {
//        holder.setEvent(data[position], onEventClick)
    }
}
