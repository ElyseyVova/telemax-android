package ru.telemax.app.ui.custom

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.MotionEvent.*
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.*
import android.widget.FrameLayout

import com.google.android.gms.maps.SupportMapFragment

class WorkaroundMapFragment : SupportMapFragment() {

    lateinit var listener: () -> Unit

    override fun onCreateView(layoutInflater: LayoutInflater, viewGroup: ViewGroup?, savedInstance: Bundle?): View? {
        val layout = super.onCreateView(layoutInflater, viewGroup, savedInstance)
        val frameLayout = TouchableWrapper(activity!!)
        frameLayout.setBackgroundColor(resources.getColor(android.R.color.transparent))

        (layout as ViewGroup).addView(frameLayout, ViewGroup.LayoutParams(MATCH_PARENT, MATCH_PARENT))

        return layout
    }

    inner class TouchableWrapper(context: Context) : FrameLayout(context) {

        override fun dispatchTouchEvent(event: MotionEvent): Boolean {
            when (event.action) {
                ACTION_DOWN -> listener.invoke()
                ACTION_UP -> listener.invoke()
            }
            return super.dispatchTouchEvent(event)
        }
    }
}
