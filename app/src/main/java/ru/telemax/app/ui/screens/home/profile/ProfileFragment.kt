package ru.telemax.app.ui.screens.home.profile

import android.Manifest
import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.android.synthetic.main.fragment_profile.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.act
import ru.telemax.app.R
import ru.telemax.app.base.BaseFragment
import ru.telemax.app.data.model.Profile
import ru.telemax.app.di.Scopes
import ru.telemax.app.ui.screens.home.profile.ProfilePresenter.Action.*
import toothpick.Toothpick


class ProfileFragment : BaseFragment(), ProfileView {

    companion object {
        fun newInstance() = ProfileFragment()
    }

    @InjectPresenter
    lateinit var presenter: ProfilePresenter

    // =============================================================================================
    //   Android
    // =============================================================================================

    override val layoutResId = R.layout.fragment_profile

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
    }

    override fun onResume() {
        super.onResume()
        presenter.fetchInfo()
    }

    // =============================================================================================
    //   View
    // =============================================================================================

    override fun setProfileInfo(profile: Profile) {
        tvNameOrg.text = profile.subscriber
        tvTreatyNumber.text = profile.number
        tvPersonManager.text = profile.manager
    }

    override fun showInfoSmall() {
        viewInfoFull.visibility = View.GONE
        btnFullInfo.setImageDrawable(resources.getDrawable(R.drawable.ic_down))
    }

    override fun showInfoFull() {
        viewInfoFull.visibility = View.VISIBLE
        btnFullInfo.setImageDrawable(resources.getDrawable(R.drawable.ic_up))
    }

    override fun requestPermission(action: ProfilePresenter.Action) {
        RxPermissions(act)
                .request(Manifest.permission.CALL_PHONE)
                .subscribe {
                    if (it) {
                        presenter.handleActionClick(action)
                    } else {
                        presenter.handleDisablePermission()
                    }
                }
    }

    // =============================================================================================
    //   Private
    // =============================================================================================

    private fun setupUI() {
        topGroup.onClick { presenter.handleShowInfoClick(viewInfoFull.isShown) }
        btnFullInfo.onClick { presenter.handleShowInfoClick(viewInfoFull.isShown) }

        btnGoSite.onClick { presenter.handleActionClick(SITE_MAIN) }
        btnCallManager.onClick { presenter.handleCallClick(CALL_MANAGER) }
        btnCallTech.onClick { presenter.handleCallClick(CALL_TECH) }
        btnRequestTech.onClick { presenter.handleActionClick(EMAIL_REQUEST) }
        btnCallCommerce.onClick { presenter.handleCallClick(CALL_COMMERCE) }
        btnRequestMe.onClick { presenter.handleActionClick(EMAIL_REQUEST_ME) }
        btnPayment.onClick { presenter.handleActionClick(SITE_PAY) }
        btnWhatsApp.onClick { presenter.handleActionClick(WHATS_APP) }
    }

    // =============================================================================================
    //   Moxy
    // =============================================================================================

    @ProvidePresenter
    fun providePresenter() = Toothpick.openScope(Scopes.APP).getInstance(ProfilePresenter::class.java)
}
