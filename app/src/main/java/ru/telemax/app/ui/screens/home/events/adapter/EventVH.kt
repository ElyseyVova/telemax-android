package ru.telemax.app.ui.screens.home.events.adapter

import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_event.view.*
import org.jetbrains.anko.layoutInflater
import org.jetbrains.anko.sdk25.coroutines.onClick
import ru.telemax.app.R
import ru.telemax.app.data.model.Event
import ru.telemax.app.util.extensions.asDate
import ru.telemax.app.util.extensions.asShortDate
import android.text.style.UnderlineSpan
import android.text.SpannableString



/**
 * Created by Elyseev Vladimir on 30.03.18.
 */
class EventVH(parent: ViewGroup) : RecyclerView.ViewHolder(parent.context.layoutInflater.inflate(R.layout.item_event, parent, false)) {

    lateinit var event: Event

    fun bind(event: Event) {
        this.event = event

        itemView?.apply {
            tvTitle.text = Html.fromHtml(event.title)
            tvDate.text = event.date.asShortDate
            tvDescription.text = Html.fromHtml(event.body)
            tvBasic.text = Html.fromHtml(event.basicText)

            if (event.isExpanded) {
                tvBasic.visibility = View.VISIBLE
            } else {
                tvBasic.visibility = View.GONE
            }

            setTextDetails()

            btnDetails.onClick {
                event.isExpanded = !event.isExpanded
                setTextDetails()
                when {
                    tvBasic.isShown -> tvBasic.visibility = View.GONE
                    else -> tvBasic.visibility = View.VISIBLE
                }

            }
        }
    }

    private fun View.setTextDetails() {
        val text = itemView.context.resources.getString(
                if (event.isExpanded) R.string.events_btn_close_details
                else R.string.events_btn_details)

        val content = SpannableString(text)
        content.setSpan(UnderlineSpan(), 0, text.length, 0)

        btnDetails.text = content
    }

}
