package ru.telemax.app.ui.screens.home

import com.arellomobile.mvp.MvpView

interface HomeView : MvpView {
    fun showNews()
}
