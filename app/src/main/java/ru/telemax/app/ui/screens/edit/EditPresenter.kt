package ru.telemax.app.ui.screens.edit

import android.content.res.Resources
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.telemax.app.R
import ru.telemax.app.data.model.Profile
import ru.telemax.app.data.repository.profile.ProfileRepository
import ru.telemax.app.navigation.Screen
import ru.telemax.app.navigation.Screen.HOME_SCREEN
import ru.telemax.app.util.extensions.navigateTo
import ru.telemax.app.util.extensions.newRootScreen
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class EditPresenter @Inject constructor(
        private val profileRepository: ProfileRepository,
        private val router: Router,
        private val resources: Resources) : MvpPresenter<EditView>() {

    private var profile: Profile = profileRepository.profile

    private var isAuthState = true

    init {
        viewState.setProfileInfo(profile)
    }

    fun setAuthState(isAuth: Boolean) {
        isAuthState = isAuth
    }

    fun handleNextClick(subscriber: String, name: String, email: String, phone: String, number: String) {
        if (subscriber.isEmpty() || name.isEmpty() || email.isEmpty() || phone.isEmpty() || number.isEmpty()) {
            router.showSystemMessage(resources.getString(R.string.auth_error_empty_field))
        } else {
            profile.subscriber = subscriber
            profile.name = name
            profile.email = email
            profile.phone = phone
            profile.number = number

            viewState.showNextAuth()
        }
    }

    fun handleOkClick(manager: String, phoneManager: String) {
        if (manager.isEmpty() || phoneManager.isEmpty()) {
            router.showSystemMessage(resources.getString(R.string.auth_error_empty_field))
        } else {
            profile.manager = manager
            profile.phoneManager = phoneManager

            profileRepository.profile = profile

            if (isAuthState) {
                router.newRootScreen(HOME_SCREEN, false)
            } else {
                router.exit()
            }
        }
    }

    fun handlePhoneClick() {
        viewState.requestPermission()
    }

    fun handleDonePermission() {
        router.navigateTo(Screen.CALL_SCREEN, "+7(861) 212–50–00")
    }

    fun handleDisablePermission() {
        router.showSystemMessage("Для звонка необходимо включить разрешение в настройках")
    }

    fun handleBackClick() {
        router.exit()
    }
}
