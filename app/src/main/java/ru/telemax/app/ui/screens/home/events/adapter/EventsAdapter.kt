package ru.telemax.app.ui.screens.home.events.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import ru.telemax.app.data.model.Event

/**
 * Created by Elyseev Vladimir on 30.03.18.
 */
class EventsAdapter : RecyclerView.Adapter<EventVH>() {

    private var events = mutableListOf<Event>()

    override fun getItemCount() = events.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = EventVH(parent)

    override fun onBindViewHolder(holder: EventVH, position: Int) {
        holder.bind(events[position])
    }

    fun setEvents(newEvents: List<Event>) {
        events = newEvents.toMutableList()
        notifyDataSetChanged()
    }

    fun addEvents(newEvents: List<Event>) {
        var oldSize = events.size
        events.addAll(newEvents)
        notifyItemRangeInserted(oldSize, events.size)
    }

}
