package ru.telemax.app.ui.screens.home.events

import com.arellomobile.mvp.MvpView
import ru.telemax.app.data.model.Event

interface EventsView : MvpView {
    fun setEvents(events: List<Event>)
    fun addEvents(events: List<Event>)
    fun showMessageViewError(isWorks: Boolean)
    fun hideMessageView()
    fun showLoader()
    fun hideLoader()

}
