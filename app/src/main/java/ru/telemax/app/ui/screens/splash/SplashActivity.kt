package ru.telemax.app.ui.screens.splash

import android.os.Bundle
import android.support.v4.app.Fragment
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import org.jetbrains.anko.toast
import ru.telemax.app.R
import ru.telemax.app.data.repository.auth.AuthRepository
import ru.telemax.app.di.Scopes
import ru.telemax.app.navigation.Screen.*
import ru.telemax.app.ui.screens.edit.EditActivity
import ru.telemax.app.ui.screens.home.HomeActivity
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.SupportAppNavigator
import toothpick.Toothpick
import javax.inject.Inject

class SplashActivity : MvpAppCompatActivity(), SplashView {

    @Inject
    lateinit var holder: NavigatorHolder

    @InjectPresenter
    lateinit var presenter: SplashPresenter

    private val navigator = object : SupportAppNavigator(this, supportFragmentManager, R.id.container) {

        override fun createActivityIntent(screenKey: String, data: Any?) = when (valueOf(screenKey)) {
            HOME_SCREEN -> HomeActivity.intent(this@SplashActivity, data as Boolean)
            EDIT_SCREEN -> EditActivity.intent(this@SplashActivity, true)
            else -> null
        }

        override fun createFragment(screenKey: String, data: Any?): Fragment? = null

        override fun showSystemMessage(message: String) {
            toast(message)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Toothpick.inject(this, Toothpick.openScope(Scopes.APP))
        
        presenter.handleFrom(intent.getBooleanExtra("from_notification", false))
    }

    override fun onResume() {
        super.onResume()
        holder.setNavigator(navigator)
    }

    override fun onPause() {
        holder.removeNavigator()
        super.onPause()
    }

    // =============================================================================================
    //   Moxy
    // =============================================================================================

    @ProvidePresenter
    fun providePresenter() = Toothpick.openScope(Scopes.APP).getInstance(SplashPresenter::class.java)
}
