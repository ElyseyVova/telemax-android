package ru.telemax.app.ui.screens.splash

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.telemax.app.data.repository.auth.AuthRepository
import ru.telemax.app.navigation.Screen.*
import ru.telemax.app.util.extensions.newRootScreen
import ru.telemax.app.util.extensions.replaceScreen
import ru.telemax.app.util.extensions.timer
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class SplashPresenter @Inject constructor(
        private val authRepository: AuthRepository,
        private val router: Router
) : MvpPresenter<SplashView>() {

    fun handleFrom(fromNotification: Boolean) {
        timer(1000) {
            when {
                authRepository.isFirstLaunch -> router.replaceScreen(PROMO_SCREEN)
                authRepository.isNotAuthorized -> router.replaceScreen(EDIT_SCREEN)
                else -> router.newRootScreen(HOME_SCREEN, fromNotification)
            }
        }
    }
}
