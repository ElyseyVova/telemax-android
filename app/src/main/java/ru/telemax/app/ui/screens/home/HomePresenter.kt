package ru.telemax.app.ui.screens.home

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.telemax.app.navigation.Screen
import ru.telemax.app.util.extensions.navigateTo
import ru.telemax.app.util.extensions.newRootScreen
import ru.telemax.app.util.extensions.replaceScreen
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class HomePresenter @Inject constructor(
        private val router: Router
) : MvpPresenter<HomeView>() {

    fun handleFrom(fromNotification: Boolean) {
        when {
            fromNotification -> router.replaceScreen(Screen.NEWS_SCREEN).also { viewState.showNews() }
            else -> router.replaceScreen(Screen.PROFILE_SCREEN)
        }
    }

    fun handleItemClick(screen: Screen) {
        router.replaceScreen(screen)
    }

    fun handleEditClick() {
        router.navigateTo(Screen.EDIT_SCREEN)
    }

    fun handleShareClick() {
        router.navigateTo(Screen.SHARE_SCREEN, "test share")
    }
}
