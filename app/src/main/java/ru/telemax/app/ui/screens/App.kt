package ru.telemax.app.ui.screens

import android.app.Application
import com.google.firebase.messaging.FirebaseMessaging
import ru.telemax.app.util.DependencyInjection


/**
 * Created by Elyseev Vladimir on 29.03.18.
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        DependencyInjection.configure(this)
        FirebaseMessaging.getInstance().subscribeToTopic("telemax_notifications")
    }
}
