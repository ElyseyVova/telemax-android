package ru.telemax.app.ui.screens.home.events

import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_events.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.onRefresh
import ru.telemax.app.R
import ru.telemax.app.base.BaseFragment
import ru.telemax.app.data.model.Event
import ru.telemax.app.di.Scopes
import ru.telemax.app.ui.screens.home.events.adapter.EventsAdapter
import toothpick.Toothpick

class EventsFragment : BaseFragment(), EventsView {

    @InjectPresenter
    lateinit var presenter: EventsPresenter

    companion object {
        fun newInstance() = EventsFragment()
    }

    private var adapter = EventsAdapter()

    // =============================================================================================
    //   Android
    // =============================================================================================

    override val layoutResId = R.layout.fragment_events

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
    }

    override fun onResume() {
        super.onResume()
        presenter.fetchStatus()
        presenter.fetchEvents()
    }

    // =============================================================================================
    //   View
    // =============================================================================================


    override fun setEvents(events: List<Event>) {
        rvEvents.clearIndexes()
        adapter.setEvents(events)
    }

    override fun addEvents(events: List<Event>) {
        adapter.addEvents(events)
    }

    override fun showMessageViewError(isWorks: Boolean) {
        viewMessage.visibility = if (isWorks) View.VISIBLE else View.GONE
        viewMessage.backgroundColor = resources.getColor(R.color.error_color_material)
        tvMessage.text = resources.getString(R.string.events_accidents)
    }

    override fun hideMessageView() {
        viewMessage.visibility = View.GONE
    }

    override fun showLoader() {
        refresh.isRefreshing = true
    }

    override fun hideLoader() {
        refresh.isRefreshing = false
    }

    private fun setupUI() {
        rvEvents.adapter = adapter
        rvEvents.onLoadMore { presenter.fetchMoreEvents() }
        btnClose.onClick { presenter.handleMessageCloseClick() }
        refresh.onRefresh { presenter.fetchEvents() }
    }

    // =============================================================================================
    //   Moxy
    // =============================================================================================

    @ProvidePresenter
    fun providePresenter() = Toothpick.openScope(Scopes.APP).getInstance(EventsPresenter::class.java)
}
