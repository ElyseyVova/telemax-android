package ru.telemax.app.ui.screens.home.contacts

import android.Manifest
import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.android.synthetic.main.fragment_contacts.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.act
import ru.telemax.app.R
import ru.telemax.app.base.BaseFragment
import ru.telemax.app.di.Scopes
import ru.telemax.app.ui.custom.WorkaroundMapFragment
import ru.telemax.app.ui.screens.home.contacts.ContactsPresenter.Action.*
import ru.telemax.app.ui.screens.home.profile.ProfilePresenter
import timber.log.Timber
import toothpick.Toothpick


class ContactsFragment : BaseFragment(), ContactsView, OnMapReadyCallback {

    companion object {
        fun newInstance() = ContactsFragment()
    }

    @InjectPresenter
    lateinit var presenter: ContactsPresenter

    // =============================================================================================
    //   Android
    // =============================================================================================

    override val layoutResId: Int = R.layout.fragment_contacts

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
    }

    override fun onMapReady(map: GoogleMap) {
        val coordinate = LatLng(45.046724, 38.974684)
        map.uiSettings.isCompassEnabled = false
        map.uiSettings.isTiltGesturesEnabled = false
        map.uiSettings.isZoomControlsEnabled = false
        map.addMarker(MarkerOptions().position(coordinate).title("Мы находимся тут"))
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 15f))
    }

    override fun requestPermission(action: ContactsPresenter.Action) {
        RxPermissions(act)
                .request(Manifest.permission.CALL_PHONE)
                .subscribe {
                    if (it) {
                        presenter.handleActionClick(action)
                    } else {
                        presenter.handleDisablePermission()
                    }
                }
    }

    // =============================================================================================
    //   View
    // =============================================================================================

    private fun setupUI() {
        btnOpenAddress.onClick { presenter.handleActionClick(ADDRESS) }
        btnOpenEmail.onClick { presenter.handleActionClick(EMAIL) }
        btnOpenSite.onClick { presenter.handleActionClick(SITE) }
        btnCallCommerce.onClick { presenter.handleCallClick(CALL_COMMERCE) }
        btnCallTech.onClick { presenter.handleCallClick(CALL_TECH) }
        btnOpenFacebook.onClick { presenter.handleActionClick(FACEBOOK) }
        btnOpenInstagram.onClick { presenter.handleActionClick(INSTAGRAMM) }

        setupMap()
    }

    private fun setupMap() {
        Thread {
            try {
                val mapFragment = WorkaroundMapFragment().also { it.listener = { sv.requestDisallowInterceptTouchEvent(true) } }
                childFragmentManager.beginTransaction().replace(R.id.map, mapFragment).commit()
                activity?.runOnUiThread { mapFragment.getMapAsync(this) }
            } catch (ignored: Exception) {
                Timber.w("Dummy map load exception: %s", ignored)
            }
        }.start()
    }

    // =============================================================================================
    //   Moxy
    // =============================================================================================

    @ProvidePresenter
    fun providePresenter() = Toothpick.openScope(Scopes.APP).getInstance(ContactsPresenter::class.java)
}
