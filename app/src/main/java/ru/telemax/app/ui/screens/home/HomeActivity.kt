package ru.telemax.app.ui.screens.home

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_home.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast
import ru.telemax.app.R
import ru.telemax.app.di.Scopes
import ru.telemax.app.navigation.Screen.*
import ru.telemax.app.ui.screens.edit.EditActivity
import ru.telemax.app.ui.screens.home.contacts.ContactsFragment
import ru.telemax.app.ui.screens.home.events.EventsFragment
import ru.telemax.app.ui.screens.home.feedback.FeedbackFragment
import ru.telemax.app.ui.screens.home.profile.ProfileFragment
import ru.telemax.app.util.Intents
import ru.telemax.app.util.removeShiftMode
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.SupportAppNavigator
import toothpick.Toothpick
import javax.inject.Inject


class HomeActivity : MvpAppCompatActivity(), HomeView {

    companion object {
        fun intent(context: Context, fromNotification: Boolean) = context.intentFor<HomeActivity>("from_notification" to fromNotification)
    }

    @Inject
    lateinit var holder: NavigatorHolder

    @InjectPresenter
    lateinit var presenter: HomePresenter

    private val navigator = object : SupportAppNavigator(this, supportFragmentManager, R.id.container) {

        override fun createActivityIntent(screenKey: String, data: Any?) = when (valueOf(screenKey)) {
            WEB_SCREEN -> Intents.openUrl(data as String)
            GOOGLE_SCREEN -> Intents.openGoogle(data as String)
            CALL_SCREEN -> Intents.openCall(data as String)
            EMAIL_TECH_SCREEN -> Intents.openEmailTech(data as String)
            EMAIL_FEEDBACK_SCREEN -> Intents.openEmailFeedback(data as Pair<String, String>)
            EMAIL_SALES_SCREEN -> Intents.openEmailSales(data as String)
            EMAIL_REQUEST_ME_SCREEN -> Intents.openEmailSales(data as String)
            WHATS_APP_SCREEN -> Intents.openWhatsApp()
            SHARE_SCREEN -> Intents.openShare()
            EDIT_SCREEN -> EditActivity.intent(this@HomeActivity, false)
            else -> null
        }

        override fun createFragment(screenKey: String, data: Any?): Fragment? = when (valueOf(screenKey)) {
            PROFILE_SCREEN -> ProfileFragment.newInstance()
            NEWS_SCREEN -> EventsFragment.newInstance()
            CONTACTS_SCREEN -> ContactsFragment.newInstance()
            FEEDBACK_SCREEN -> FeedbackFragment.newInstance()
            else -> null
        }

        override fun showSystemMessage(message: String) {
            toast(message)
        }

        override fun exit() {
            finish()
        }
    }

    // =============================================================================================
    // Android
    // =============================================================================================

    override fun onResume() {
        super.onResume()
        holder.setNavigator(navigator)
    }

    override fun onPause() {
        holder.removeNavigator()
        super.onPause()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        Toothpick.inject(this, Toothpick.openScope(Scopes.APP))
        setupToolbar()
        setupNavigation()

        presenter.handleFrom(intent.getBooleanExtra("from_notification", false))
    }

    override fun showNews() {
        navigation.selectedItemId = R.id.navigation_news
    }

    // =============================================================================================
    // Private
    // =============================================================================================

    private fun setupToolbar() {
        toolbar.apply {
            title = resources.getString(R.string.profile_title)
            inflateMenu(R.menu.home)
            menu.findItem(R.id.navigation_edit).isVisible = true
            setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.navigation_edit -> presenter.handleEditClick()
                    R.id.navigation_share -> presenter.handleShareClick()
                }
                true
            }
        }
    }

    private fun setupNavigation() {
        navigation.removeShiftMode()
        navigation.itemIconTintList = null
        navigation.setOnNavigationItemSelectedListener {
            toolbar.title = it.title
            toolbar.menu.findItem(R.id.navigation_edit).isVisible = it.itemId == R.id.navigation_profile
            toolbar.menu.findItem(R.id.navigation_share).isVisible = it.itemId == R.id.navigation_contacts

            presenter.handleItemClick(
                    when (it.itemId) {
                        R.id.navigation_profile -> PROFILE_SCREEN
                        R.id.navigation_news -> NEWS_SCREEN
                        R.id.navigation_contacts -> CONTACTS_SCREEN
                        R.id.navigation_feedback -> FEEDBACK_SCREEN
                        else -> PROFILE_SCREEN
                    }
            )
            true
        }
    }

    // =============================================================================================
    //   Moxy
    // =============================================================================================

    @ProvidePresenter
    fun providePresenter() = Toothpick.openScope(Scopes.APP).getInstance(HomePresenter::class.java)
}
