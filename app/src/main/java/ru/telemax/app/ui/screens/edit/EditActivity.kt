package ru.telemax.app.ui.screens.edit

import android.Manifest
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import com.tbruyelle.rxpermissions2.RxPermissions
import kotlinx.android.synthetic.main.activity_edit.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.act
import org.jetbrains.anko.toast
import ru.telemax.app.R
import ru.telemax.app.data.model.Profile
import ru.telemax.app.di.Scopes
import ru.telemax.app.navigation.Screen
import ru.telemax.app.ui.screens.home.HomeActivity
import ru.telemax.app.ui.screens.home.profile.ProfilePresenter
import ru.telemax.app.util.Intents
import ru.telemax.app.util.extensions.addMaskNumber
import ru.telemax.app.util.extensions.addMaskPhone
import ru.telemax.app.util.extensions.text
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.SupportAppNavigator
import toothpick.Toothpick
import javax.inject.Inject

class EditActivity : MvpAppCompatActivity(), EditView {

    companion object {
        val EXTRA_IS_AUTH = "extra.is_auth"

        fun intent(context: Context, isAuth: Boolean) = context.intentFor<EditActivity>(EXTRA_IS_AUTH to isAuth)
    }

    @Inject
    lateinit var holder: NavigatorHolder

    @InjectPresenter
    lateinit var presenter: EditPresenter

    private var isAuth: Boolean = true

    private val navigator = object : SupportAppNavigator(this, supportFragmentManager, R.id.container) {

        override fun createActivityIntent(screenKey: String, data: Any?) = when (Screen.valueOf(screenKey)) {
            Screen.HOME_SCREEN -> HomeActivity.intent(this@EditActivity, data as Boolean)
            Screen.CALL_SCREEN -> Intents.openCall(data as String)
            else -> null
        }

        override fun createFragment(screenKey: String, data: Any?): Fragment? = null

        override fun showSystemMessage(message: String) {
            toast(message)
        }

        override fun exit() {
            finish()
        }
    }

    // =============================================================================================
    //   Android
    // =============================================================================================

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)
        Toothpick.inject(this, Toothpick.openScope(Scopes.APP))

        fetchExtra()
        setupToolbar()
        setupUI()
    }

    override fun onResume() {
        super.onResume()
        holder.setNavigator(navigator)
    }

    override fun onPause() {
        holder.removeNavigator()
        super.onPause()
    }

    override fun onBackPressed() {
        presenter.handleBackClick()
    }

    // =============================================================================================
    //   View
    // =============================================================================================

    override fun setProfileInfo(profile: Profile) {
        etNameSubscriber.setText(profile.subscriber)
        etNamePerson.setText(profile.name)
        etEmail.setText(profile.email)
        etPhone.setText(profile.phone)
        etNumberTreaty.setText(profile.number)
        etPersonManager.setText(profile.manager)
        etPhonePersonManager.setText(profile.phoneManager)
    }

    override fun showNextAuth() {
        viewAuth1.visibility = View.GONE
        viewAuth2.visibility = View.VISIBLE

        btnNext.text = resources.getString(R.string.ok)

        btnNext.onClick { processOkClick() }
    }

    override fun requestPermission() {
        RxPermissions(this)
                .request(Manifest.permission.CALL_PHONE)
                .subscribe {
                    if (it) presenter.handleDonePermission() else presenter.handleDisablePermission()
                }
    }

    private fun fetchExtra() {
        isAuth = intent.extras.getBoolean(EXTRA_IS_AUTH, true)
        presenter.setAuthState(isAuth)
    }

    private fun setupToolbar() {
        if (!isAuth) {
            toolbar.title = getString(R.string.edit_title)
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back)
            toolbar.setNavigationOnClickListener { presenter.handleBackClick() }
        }
    }

    private fun setupUI() {
        etPhone.addMaskPhone()
        etPhonePersonManager.addMaskPhone()

        btnNext.onClick { processNextClick() }
        tvPhone.onClick { presenter.handlePhoneClick() }
    }

    private fun processNextClick() {
        val subscriber = etNameSubscriber.text()
        val name = etNamePerson.text()
        val email = etEmail.text()
        val phone = etPhone.text()
        val number = etNumberTreaty.text()

        presenter.handleNextClick(subscriber, name, email, phone, number)
    }

    private fun processOkClick() {
        val manager = etPersonManager.text()
        val phoneManager = etPhonePersonManager.text()

        presenter.handleOkClick(manager, phoneManager)
    }

    // =============================================================================================
    //   Moxy
    // =============================================================================================

    @ProvidePresenter
    fun providePresenter() = Toothpick.openScope(Scopes.APP).getInstance(EditPresenter::class.java)
}
