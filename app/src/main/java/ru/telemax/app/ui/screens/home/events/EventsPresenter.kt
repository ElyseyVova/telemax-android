package ru.telemax.app.ui.screens.home.events

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import io.reactivex.disposables.CompositeDisposable
import ru.telemax.app.data.model.Event
import ru.telemax.app.data.repository.events.EventsRepository
import ru.telemax.app.data.repository.profile.ProfileRepository
import javax.inject.Inject

@InjectViewState
class EventsPresenter @Inject constructor(
    private val profileRepository: ProfileRepository,
    private val eventsRepository: EventsRepository
) : MvpPresenter<EventsView>() {

    private var disposable = CompositeDisposable()

    private var page = 1

    fun fetchStatus() {
        profileRepository.getStatus(
            onStart = {  },
            onSuccess = { viewState.showMessageViewError(it) },
            onError = {},
            onFinally = { },
            addDisposables = disposable)
    }

    fun fetchEvents() {
        page = 1
        eventsRepository.getEvents(page,
                onStart = { viewState.showLoader() },
                onSuccess = { viewState.setEvents(it) },
                onError = {},
                onFinally = { viewState.hideLoader()},
                addDisposables = disposable)
    }

    fun fetchMoreEvents() {
        page++
        eventsRepository.getEvents(page,
                onStart = { viewState.showLoader() },
                onSuccess = { proceedResult(it) },
                onError = {},
                onFinally = { viewState.hideLoader() },
                addDisposables = disposable)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.clear()
    }


    fun handleMessageCloseClick() {
        viewState.hideMessageView()
    }

    private fun proceedResult(it: List<Event>) {
        if (it.isEmpty()) page--
        viewState.addEvents(it)
    }
}
