package ru.telemax.app.ui.screens.edit

import com.arellomobile.mvp.MvpView
import ru.telemax.app.data.model.Profile
import ru.telemax.app.ui.screens.home.profile.ProfilePresenter

interface EditView: MvpView {
    fun setProfileInfo(profile: Profile)
    fun showNextAuth()
    fun requestPermission()
}
