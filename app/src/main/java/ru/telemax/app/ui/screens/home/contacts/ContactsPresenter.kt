package ru.telemax.app.ui.screens.home.contacts

import android.content.res.Resources
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.telemax.app.R
import ru.telemax.app.navigation.Screen.*
import ru.telemax.app.ui.screens.home.contacts.ContactsPresenter.Action.*
import ru.telemax.app.ui.screens.home.profile.ProfilePresenter
import ru.telemax.app.util.extensions.navigateTo
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class ContactsPresenter @Inject constructor(
        private val router: Router,
        private val resources: Resources
) : MvpPresenter<ContactsView>() {

    fun handleActionClick(action: Action) {
        when (action) {
            ADDRESS -> router.navigateTo(GOOGLE_SCREEN, resources.getString(R.string.contacts_address_info))
            EMAIL -> router.navigateTo(EMAIL_SALES_SCREEN, "")
            SITE -> router.navigateTo(WEB_SCREEN, resources.getString(R.string.contacts_site_info))
            CALL_COMMERCE -> router.navigateTo(CALL_SCREEN, resources.getString(R.string.contacts_phone_commercial_info))
            CALL_TECH -> router.navigateTo(CALL_SCREEN, resources.getString(R.string.contacts_phone_tech_info))
            FACEBOOK -> router.navigateTo(WEB_SCREEN, resources.getString(R.string.contacts_facebook_info))
            INSTAGRAMM -> router.navigateTo(WEB_SCREEN, resources.getString(R.string.contacts_instagram_info))
        }
    }

    fun handleCallClick(action: Action) {
        viewState.requestPermission(action)
    }

    fun handleDisablePermission() {
        router.showSystemMessage("Для звонка необходимо включить разрешение в настройках")
    }

    enum class Action {
        ADDRESS, EMAIL, SITE, CALL_COMMERCE, CALL_TECH, FACEBOOK, INSTAGRAMM
    }
}
