package ru.telemax.app.ui.screens.splash.promo

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter

@InjectViewState
class PromoPresenter : MvpPresenter<PromoView>()
