package ru.telemax.app.ui.screens.home.feedback

import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.fragment_feedback.*
import org.jetbrains.anko.inputMethodManager
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.act
import ru.telemax.app.R
import ru.telemax.app.base.BaseFragment
import ru.telemax.app.di.Scopes
import ru.telemax.app.util.extensions.text
import toothpick.Toothpick

class FeedbackFragment : BaseFragment(), FeedbackView {

    companion object {
        fun newInstance() = FeedbackFragment()
    }

    @InjectPresenter
    lateinit var presenter: FeedbackPresenter

    // =============================================================================================
    //   Android
    // =============================================================================================

    override val layoutResId: Int = R.layout.fragment_feedback

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
    }

    // =============================================================================================
    //   View
    // =============================================================================================

    private fun setupUI() {
        viewFeedback.onClick {
            edFeedback.requestFocus()
            act.inputMethodManager.showSoftInput(edFeedback, 0)
        }

        btnSendFeedback.onClick { presenter.handleSendClick(edFeedback.text()) }
        btnOpenForm.onClick { presenter.handleFormClick() }
    }

    // =============================================================================================
    //   Moxy
    // =============================================================================================

    @ProvidePresenter
    fun providePresenter() = Toothpick.openScope(Scopes.APP).getInstance(FeedbackPresenter::class.java)
}
