package ru.telemax.app.ui.screens.home.profile

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import ru.telemax.app.data.model.Profile
import ru.telemax.app.data.repository.profile.ProfileRepository
import ru.telemax.app.navigation.Screen
import ru.telemax.app.ui.screens.home.profile.ProfilePresenter.Action.*
import ru.telemax.app.util.extensions.navigateTo
import ru.terrakok.cicerone.Router
import javax.inject.Inject

@InjectViewState
class ProfilePresenter @Inject constructor(
        private val profileRepository: ProfileRepository,
        private val router: Router
) : MvpPresenter<ProfileView>() {

    private lateinit var profile: Profile

    fun fetchInfo() {
        profile = profileRepository.profile
        viewState.setProfileInfo(profile)
    }

    fun handleShowInfoClick(isShown: Boolean) {
        if (isShown) viewState.showInfoSmall() else viewState.showInfoFull()
    }

    fun handleActionClick(action: Action) {
        when (action) {
            SITE_MAIN -> router.navigateTo(Screen.WEB_SCREEN, "http://hitel.ru")
            CALL_MANAGER -> router.navigateTo(Screen.CALL_SCREEN, profile.phoneManager)
            CALL_TECH -> router.navigateTo(Screen.CALL_SCREEN, "+7 (861) 212-50-00")
            EMAIL_REQUEST_ME -> router.navigateTo(Screen.EMAIL_TECH_SCREEN, profile.subscriber)
            CALL_COMMERCE -> router.navigateTo(Screen.CALL_SCREEN, "+7 (861) 212-55-55")
            EMAIL_REQUEST -> router.navigateTo(Screen.EMAIL_TECH_SCREEN, "Заявка в СП контакт ${profile.subscriber}, ${profile.phone} мобильное приложение ")
            SITE_PAY -> router.navigateTo(Screen.WEB_SCREEN, "http://hitel.ru/pages/payment.html")
            WHATS_APP -> router.navigateTo(Screen.WHATS_APP_SCREEN, "")
        }
    }

    fun handleCallClick(action: Action) {
        viewState.requestPermission(action)
    }

    fun handleDisablePermission() {
        router.showSystemMessage("Для звонка необходимо включить разрешение в настройках")
    }

    enum class Action {
        SITE_MAIN, CALL_MANAGER, CALL_TECH, EMAIL_REQUEST,
        CALL_COMMERCE, EMAIL_REQUEST_ME, SITE_PAY, WHATS_APP
    }
}
