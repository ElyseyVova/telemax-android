package ru.telemax.app.ui.screens.home.profile

import com.arellomobile.mvp.MvpView
import ru.telemax.app.data.model.Profile

interface ProfileView : MvpView {
    fun showInfoSmall()
    fun showInfoFull()
    fun setProfileInfo(profile: Profile)
    fun requestPermission(action: ProfilePresenter.Action)

}
