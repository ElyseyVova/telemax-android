package ru.telemax.app.ui.screens.home.presentation.presenter;

import ru.telemax.app.R;
import ru.telemax.app.ui.screens.home.presentation.view.HomeView;
import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

@InjectViewState
public class HomePresenter extends MvpPresenter<HomeView>  {

}
