package ru.telemax.app.ui.screens.home.contacts

import com.arellomobile.mvp.MvpView
import ru.telemax.app.ui.screens.home.profile.ProfilePresenter

interface ContactsView : MvpView {
    fun requestPermission(action: ContactsPresenter.Action)
}
