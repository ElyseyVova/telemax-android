package ru.telemax.app.ui.custom

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet

class InfiniteRecyclerView @JvmOverloads constructor(ctx: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    RecyclerView(ctx, attrs, defStyleAttr) {

    private var listenerOnLoadMore = {}
    private var previousTotal = 0
    private var isLoading = true

    override fun onScrolled(dx: Int, dy: Int) {
        super.onScrolled(dx, dy)

        val totalItemCount = layoutManager.itemCount
        val firstVisibleItem = (layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
        val visibleThreshold = 3

        if (isLoading) {
            if (totalItemCount > previousTotal) {
                isLoading = false
                previousTotal = totalItemCount
            }
        }

        if (!isLoading && dy != 0 && totalItemCount - childCount <= firstVisibleItem + visibleThreshold) {
            listenerOnLoadMore.invoke()
            isLoading = true
        }
    }

    fun clearIndexes() {
        previousTotal = 0
    }

    fun onLoadMore(listener: () -> Unit) {
        listenerOnLoadMore = listener
    }
}
