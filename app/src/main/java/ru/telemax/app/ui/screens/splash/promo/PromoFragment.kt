package ru.telemax.app.ui.screens.splash.promo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.arellomobile.mvp.MvpAppCompatFragment
import com.arellomobile.mvp.presenter.InjectPresenter
import kotlinx.android.synthetic.main.activity_edit.*
import org.jetbrains.anko.sdk25.coroutines.onClick

import ru.telemax.app.R

class PromoFragment : MvpAppCompatFragment(), PromoView {

    companion object {
        fun newInstance() = PromoFragment()
    }

    @InjectPresenter
    lateinit var presenter: PromoPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_promo, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
    }

    private fun setupUI() {

    }
}
