package ru.telemax.app.di

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Resources
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.telemax.app.data.repository.auth.AuthRepository
import ru.telemax.app.data.repository.auth.AuthRepositoryIml
import ru.telemax.app.data.repository.events.EventsRepository
import ru.telemax.app.data.repository.events.EventsRepositoryIml
import ru.telemax.app.data.repository.profile.ProfileRepository
import ru.telemax.app.data.repository.profile.ProfileRepositoryIml
import ru.telemax.app.data.retrofit.TelemaxApi
import ru.telemax.app.data.store.AuthStore
import ru.telemax.app.data.store.AuthStoreImpl
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router
import toothpick.config.Module

class ApplicationModule(
        context: Context,
        telemaxDomain: String,
        logLevel: HttpLoggingInterceptor.Level) : Module() {

    companion object {
        private val CALL_ADAPTER_FACTORY = RxJava2CallAdapterFactory.create()

        private var GSON = GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create()

        private val CONVERTER_FACTORY = GsonConverterFactory.create(GSON)

        private const val CONTENT_TYPE_JSON = "application/json; charset=utf-8"

        private const val FORMAT_JSON = "json"
        private const val HEADER_AUTH = "Authorization"
        private const val HEADER_CONTENT_TYPE = "Content-Type"

        private const val PREFS_NAME = "prefs"

        private const val TIMEOUT_MS_CONNECT = 600000L
        private const val TIMEOUT_MS_WRITE = 600000L
        private const val TIMEOUT_MS_READ = 600000L

        private const val QUERY_FORMAT = "format"
        private const val QUERY_KEY = "key"
    }

    private val prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

    private val authStore = AuthStoreImpl(prefs) as AuthStore

    init {
        val okhttpInterceptorLogging = HttpLoggingInterceptor().apply { level = logLevel }

        val okhttpClientTelemax = OkHttpClient.Builder()
                .addInterceptor(okhttpInterceptorLogging)
                .build()


        val telemaxApi = Retrofit.Builder()
                .addCallAdapterFactory(CALL_ADAPTER_FACTORY)
                .addConverterFactory(CONVERTER_FACTORY)
                .baseUrl("https://$telemaxDomain/")
                .client(okhttpClientTelemax)
                .build().create(TelemaxApi::class.java)

        bind(TelemaxApi::class.java).toInstance(telemaxApi)

        bind(Context::class.java).toInstance(context)
        bind(Gson::class.java).toInstance(GSON)
        bind(Resources::class.java).toInstance(context.resources)
        bind(SharedPreferences::class.java).toInstance(prefs)

        /* Store */
        bind(AuthStore::class.java).toInstance(authStore)

        /* Managers */
//        val pushManager = FirebasePushManager()
//        bind(PushManager::class.java).toInstance(pushManager)

        /* Navigation */
        val cicerone = Cicerone.create()
        bind(Router::class.java).toInstance(cicerone.router)
        bind(NavigatorHolder::class.java).toInstance(cicerone.navigatorHolder)

        /* Repository*/
        bind(AuthRepository::class.java).toInstance(AuthRepositoryIml(authStore))
        bind(ProfileRepository::class.java).toInstance(ProfileRepositoryIml(telemaxApi, authStore))
        bind(EventsRepository::class.java).toInstance(EventsRepositoryIml(telemaxApi))
    }
}
