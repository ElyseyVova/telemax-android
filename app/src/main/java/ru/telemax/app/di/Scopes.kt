package ru.telemax.app.di

object Scopes {
    const val APP = "Scopes#App"
    const val ACTIVITY_HOME = "Scopes#HomeActivity"
}
